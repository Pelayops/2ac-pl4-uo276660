#include <stdio.h>
#include <string.h>

struct _Person {
	char name[30];
	int heightcm;
	double weightkg;
};

//This abreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[]){
	Person Peter;
	strcpy(Peter.name, "Peter");
	Peter.heightcm = 175;
	Peter.weightkg = 78.7;

	printf("Peter's height: %d cm; Peter's weigt: %f",Peter.heightcm,Peter.weightkg);


	//second part of this program
	Person Javier;
	PPerson pJavier;
	

	//memory location of variable javier is assigned to the pointer
	pJavier = &Javier;
	Javier.heighcm = 180;
	Javier.weightkg = 84.0;
	pJavier ->weightkg = 83.2;
	return 0;
}

